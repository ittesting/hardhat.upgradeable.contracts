// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;
import "./BaseContract.sol";

contract AccountBalanceContract is BaseContract {
    function initializeAccountBalance() public initializer {
        BaseContract.initialize();
    }

    function accountBalance() public view returns(uint256){
        return accountBalances[accountAddress];
    }
}
