// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract BaseContract is Initializable {

    mapping(address => uint256) public accountBalances;
    address public accountAddress;

    function initialize() public onlyInitializing {
        accountAddress = msg.sender;
    }

}
