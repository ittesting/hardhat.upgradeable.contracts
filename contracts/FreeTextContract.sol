// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;
import "./BaseContract.sol";

contract FreeTextContract is BaseContract {
    string public freeText;
    function initializeFreeTextContract() public initializer {
        BaseContract.initialize();
        freeText = 'IT Testing V0';
    }

    function getFreeTextContract() external view returns(string memory){
        return freeText;
    }
}
