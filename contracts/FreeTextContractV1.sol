// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;
import "./BaseContract.sol";

contract FreeTextContractV1 is BaseContract {
    string public freeText;
    string public freeTextV1;
    function initializeFreeTextContractV2() public {
        // BaseContract.initialize();
        freeTextV1 = 'IT Testing Upgrade';
        freeText = 'IT Testing Upgrade freeText';
    }

    function getFreeTextContractV1() external view returns(string memory){
        return freeTextV1;
    }

    function getFreeTextContract() external view returns(string memory){
        return freeText;
    }
}
