// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;
import "./BaseContract.sol";

contract MyBalanceContract is BaseContract {
    function initializeMyBalance() public initializer {
        BaseContract.initialize();
    }

    function myBalance() public view returns (uint256) {
        return accountBalances[msg.sender];
    }
}
