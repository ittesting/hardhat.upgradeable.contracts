// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;
import "./BaseContract.sol";

contract TopUpContract is BaseContract {
    function initializeTopUp() public initializer {
        BaseContract.initialize();
    }
    
    function topUp(uint256 _amount) public payable returns(bool){
        accountBalances[accountAddress] += _amount;
        return true;
    }
}
