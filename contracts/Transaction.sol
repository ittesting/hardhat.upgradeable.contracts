// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract Transaction is Initializable {
    mapping(address => uint256) balances;
    address public _owner;
    uint256 public _amount;

    event Transfer(address indexed _from, address indexed _to, uint256 _value);

    function initialize(uint256 amount) public initializer {
        _amount = amount;
    }

    function transfer(address _to, uint256 _value) external {
        balances[msg.sender] -= _value;
        balances[_to] += _value;

        emit Transfer(msg.sender, _to, _value);
    }

    function balanceOf(address account) external view returns (uint256) {
        return balances[account];
    }

    function retrieve() external view returns (uint256) {
        return _amount;
    }
}
