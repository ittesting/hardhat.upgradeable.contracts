// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract TransactionV2 is Initializable {
    mapping(address => uint256) accountBalances;
    address public accountAddress;
    uint256 private _amount;

    event Transfer(address indexed _from, address indexed _to, uint256 _value);

    // Emitted when the stored value changes
    event ValueChanged(uint256 amount);

    function initialize(uint256 amount) public initializer {
        _amount = amount;
    }

    function transfer(address _to, uint256 _value) external {
        accountBalances[msg.sender] -= _value;
        accountBalances[_to] += _value;

        emit Transfer(msg.sender, _to, _value);
    }

    // Stores a new value in the contract
    function store(uint256 amount) public {
        _amount = amount;
        emit ValueChanged(amount);
    }

    function balanceOf(address account) external view returns (uint256) {
        return accountBalances[account];
    }

    function retrieve() public view returns (uint256) {
        return _amount;
    }
}
