// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;
import "./TransactionV2.sol";

contract TransactionV3 is TransactionV2 {
    string public _str;

    function initialize() public initializer {
        _str = "TransactionV3";
    }

    function getStr() public view returns (string memory) {
        return _str;
    }
}
