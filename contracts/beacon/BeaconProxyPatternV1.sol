// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;
import {Initializable} from "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import {VersionAware} from "./VersionAware.sol";

contract BeaconProxyPatternV1 is Initializable, VersionAware {

    function initialize() external initializer {
        versionAwareContractName = "Beacon Proxy Pattern: V1";
    }

    function getContractNameWithVersion()
        public
        pure
        override
        returns (string memory)
    {
        return "Beacon Proxy Pattern: V1";
    }
}
