// hardhat.config.js
require("@nomiclabs/hardhat-ethers");
require('@openzeppelin/hardhat-upgrades');

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.17",
  networks: {
    hardhat: {
      chainId: 1337 // We set 1337 to make interacting with MetaMask simpler
    },
    goerli: {
      url: 'https://eth-goerli.g.alchemy.com/v2/gUcDay6sUL-vAjwqtzTenBWBPfd1698V',
      accounts: ['0x52ce6935347d6fe6e5e0bfab4b540a44aa76c9956f90e758f8dcbac415f30123'],
    },
  },
};
// Local a29c
// 606caa62a76442fb112519231d44c322af35c2dcb39430854adb6ae2c44bdd6e
// 0x2ab9430d84963134Be46cCb694fdB7C158A21183

// Account 2
// e4584d3b519f1aff0e49955ba5f994988edd9cc53bddf78612d182c01f3fd351
// 0xCDbee2a6e3Aa086D0480550605a256537Ad73875
