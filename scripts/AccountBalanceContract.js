const { ethers, upgrades } = require("hardhat");

// scripts/deploy.js
async function main() {
  const [deployer] = await ethers.getSigners();
  console.log("Deployer account:", await deployer.getAddress());

  const AccountBalanceContract = await ethers.getContractFactory("AccountBalanceContract");
  const transaction = await upgrades.deployProxy(AccountBalanceContract, [], {
    initializer: "initializeAccountBalance",
  });

  console.log("Contract address:", transaction.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
