const { ethers, upgrades } = require("hardhat");
async function main() {
  const BeaconProxyPatternV1 = await ethers.getContractFactory(
    "BeaconProxyPatternV1"
  );
  const beacon = await upgrades.deployBeacon(BeaconProxyPatternV1, { initializer: "initialize" });
  await beacon.deployed();
  console.log(
    `Beacon with Beacon Proxy Pattern V1 as implementation is deployed to address: ${beacon.address}`
  );
  const beaconProxy1 = await upgrades.deployBeaconProxy(
    beacon.address,
    BeaconProxyPatternV1,
    []
  );
  let versionAwareContractName =
    await beaconProxy1.getContractNameWithVersion();
  console.log(
    `Proxy Pattern and Version from Proxy 1 Implementation: ${versionAwareContractName}`
  );
  
}
// We recommend this pattern to be able to use async/await everywhere and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
