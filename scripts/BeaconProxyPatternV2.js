const { ethers, upgrades } = require("hardhat");
require('dotenv').config();
const { CT_ADDRESS } = process.env;
async function main() {
  const BeaconProxyPatternV1 = await ethers.getContractFactory(
    "BeaconProxyPatternV1"
  );
  const beaconProxy2 = await upgrades.deployBeaconProxy(
    CT_ADDRESS,
    BeaconProxyPatternV1,
    []
  );
  versionAwareContractName = await beaconProxy2.getContractNameWithVersion();
  console.log(
    `Proxy Pattern and Version from Proxy 2 Implementation: ${versionAwareContractName}`
  );
  const BeaconProxyPatternV2 = await ethers.getContractFactory(
    "BeaconProxyPatternV2"
  );
  const upgradedBeacon = await upgrades.upgradeBeacon(
    CT_ADDRESS,
    BeaconProxyPatternV2,
    { unsafeAllow: ["constructor"] }
  );
  console.log(
    `Beacon upgraded with Beacon Proxy Pattern V2 as implementation at address: ${upgradedBeacon.address}`
  );
  versionAwareContractName = await beaconProxy1.getContractNameWithVersion();
  console.log(
    `Proxy Pattern and Version from Proxy 1 Implementation: ${versionAwareContractName}`
  );
  versionAwareContractName = await beaconProxy2.getContractNameWithVersion();
  console.log(
    `Proxy Pattern and Version from Proxy 2 Implementation: ${versionAwareContractName}`
  );
  versionAwareContractName = await beaconProxy1.versionAwareContractName();
  console.log(
    `Proxy Pattern and Version from Proxy 1 Storage: ${versionAwareContractName}`
  );
  versionAwareContractName = await beaconProxy2.versionAwareContractName();
  console.log(
    `Proxy Pattern and Version from Proxy 2 Storage: ${versionAwareContractName}`
  );
  const initTx = await beaconProxy1.initialize();
  const receipt = await initTx.wait();
  versionAwareContractName = await beaconProxy1.versionAwareContractName();
  console.log(
    `Proxy Pattern and Version from Proxy 1 Storage: ${versionAwareContractName}`
  );
  versionAwareContractName = await beaconProxy2.versionAwareContractName();
  console.log(
    `Proxy Pattern and Version from Proxy 2 Storage: ${versionAwareContractName}`
  );
}
// We recommend this pattern to be able to use async/await everywhere and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
