const { ethers, upgrades } = require("hardhat");

async function main() {
  const [deployer] = await ethers.getSigners();
  console.log("Deployer account:", await deployer.getAddress());

  const FreeTextContract = await ethers.getContractFactory("FreeTextContract");
  const transaction = await upgrades.deployProxy(FreeTextContract, [], {
    initializer: "initializeFreeTextContract",
  });
  await transaction.deployed();

  console.log("Contract address:", transaction.address);
  const currentImplAddress = await upgrades.erc1967.getImplementationAddress(
    transaction.address
  );
  console.log("currentImplAddress 001:", currentImplAddress);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
