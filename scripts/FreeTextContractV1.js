const { ethers, upgrades } = require("hardhat");
require('dotenv').config();
const { CT_ADDRESS } = process.env;
// scripts/deploy.js
async function main() {
  const [deployer] = await ethers.getSigners();
  console.log(
    "Deploying the contracts with the account:",
    await deployer.getAddress()
  );

  const FreeTextContractV1 = await ethers.getContractFactory("FreeTextContractV1");
  const transaction = await upgrades.upgradeProxy(CT_ADDRESS, FreeTextContractV1);
  await transaction.deployed();
  await transaction.initializeFreeTextContractV2();
  console.log("Contract address:", transaction.address);
  const currentImplAddress = await upgrades.erc1967.getImplementationAddress(
    transaction.address
  );
  console.log("currentImplAddress 002:", currentImplAddress);
  
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
