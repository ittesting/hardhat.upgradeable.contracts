const { ethers, upgrades } = require("hardhat");

// scripts/deploy.js
async function main() {
  const [deployer] = await ethers.getSigners();
  console.log("Deployer account:", await deployer.getAddress());

  const TopUpContract = await ethers.getContractFactory("TopUpContract");
  const transaction = await upgrades.deployProxy(TopUpContract, [], {
    initializer: "initializeTopUp",
  });

  console.log("Contract address:", transaction.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
