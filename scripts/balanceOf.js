const { ethers, upgrades } = require("hardhat");

async function main() {
    // Retrieve accounts from the local node
    const accounts = await ethers.provider.listAccounts();
    console.log(accounts);

    const address = '0x5FC8d32690cc91D4c39d9d3abcBD16989F875707'; // BOX_ADDRESS
    const TransactionV2 = await ethers.getContractFactory('TransactionV2');
    const transactionV2 = await TransactionV2.attach(address);
    
    const data = await transactionV2.balanceOf('0x70997970C51812dc3A010C7d01b50e0d17dc79C8');
    console.log(data.toString());
}
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });


