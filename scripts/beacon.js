const { ethers, upgrades } = require("hardhat");
require("dotenv").config();
const { CT_ADDRESS, BEACON_ADDRESS, BOX_ADDRESS } = process.env;
async function main() {
    const BeaconProxyPatternV2 = await ethers.getContractFactory(
        "BeaconProxyPatternV1"
      );
    const box = BeaconProxyPatternV2.attach(BOX_ADDRESS);
    console.log(await box.getContractNameWithVersion());
}
// We recommend this pattern to be able to use async/await everywhere and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
