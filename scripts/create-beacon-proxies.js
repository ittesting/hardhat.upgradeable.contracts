const { ethers, upgrades } = require("hardhat");

async function main() {
  const BeaconProxyPatternV1Beacon = await ethers.getContractFactory("BeaconProxyPatternV1");

  const beacon = await upgrades.deployBeacon(BeaconProxyPatternV1Beacon, { initializer: "initialize" });
  await beacon.deployed();
  console.log("BEACON_ADDRESS:", beacon.address);

  const beaconProxyPatternV1BeaconProxy = await upgrades.deployBeaconProxy(beacon, BeaconProxyPatternV1Beacon);
  await beaconProxyPatternV1BeaconProxy.deployed();
  console.log("BOX_ADDRESS:", beaconProxyPatternV1BeaconProxy.address);
}

main();