const { ethers, upgrades } = require("hardhat");
// const { changeProxyAdmin } = require("@openzeppelin/contracts-upgradeable");
// const { getManifestAdmin } = require('@openzeppelin/hardhat-upgrades');
// const { getManifestAdmin } = require('./admin');
const {getImplementationAddressFromProxy, getImplementationAddress, getAdminAddress, getManifestAdmin } = require("@openzeppelin/upgrades-core");
require('dotenv').config();
const { CT_ADDRESS, OLD_IMP_ADDRESS, NEW_IMP_ADDRESS } = process.env;

async function main() {
    const proxyAddress = CT_ADDRESS; //Contract address

    const implAddress = await getImplementationAddressFromProxy(hre.network.provider, proxyAddress);
    console.log('implAddress:', implAddress);
    
    const currentImplAddress = await upgrades.erc1967.getImplementationAddress(proxyAddress);
    console.log('currentImplAddress:', currentImplAddress);

    const adminAddress = await getAdminAddress(hre.network.provider, proxyAddress);
    console.log('adminAddress:', adminAddress);

    // const _changeProxyAdmin = await changeProxyAdmin(OLD_IMP_ADDRESS, adminAddress);
    // const aa = await makeChangeProxyAdmin(hre);

    // console.log('_changeProxyAdmin:', _changeProxyAdmin);

    const admin = await getManifestAdmin(hre);
    const proxyAdminAddress = await getAdminAddress(hre.network.provider, proxyAddress);

    if (admin.address !== proxyAdminAddress) {
      throw new Error('Proxy admin is not the one registered in the network manifest');
    } else if (admin.address !== newAdmin) {
      await admin.changeProxyAdmin(OLD_IMP_ADDRESS, adminAddress);
    }
}
//  001
// Deployer account: 0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266
// Contract address: 0xab16A69A5a8c12C732e0DEFF4BE56A70bb64c926
// currentImplAddress 001: 0x172076E0166D1F9Cc711C77Adf8488051744980C

//  002
// Deploying the contracts with the account: 0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266
// Contract address: 0xab16A69A5a8c12C732e0DEFF4BE56A70bb64c926
// currentImplAddress 002: 0xDC11f7E700A4c898AE5CAddB1082cFfa76512aDD
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });