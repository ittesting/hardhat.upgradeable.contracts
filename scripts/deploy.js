const { ethers, upgrades } = require("hardhat");

// scripts/deploy.js
async function main() {

    const [deployer] = await ethers.getSigners();
    console.log(
      "Deploying the contracts with the account:",
      await deployer.getAddress()
    );

    const Transaction = await ethers.getContractFactory('Transaction');
    const transaction = await Transaction.deploy();
    transaction.deployed();
    console.log("Transaction Deployed to:", transaction.address);
    const _balanceEther = ethers.utils.formatEther(await deployer.getBalance()).toString();
    console.log("Account balance:", _balanceEther);
  }
  
  main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });