const { ethers, upgrades } = require("hardhat");

// scripts/deploy.js
async function main() {
  const BOX_ADDRESS = "0x5FC8d32690cc91D4c39d9d3abcBD16989F875707";
  const [deployer] = await ethers.getSigners();
  console.log(
    "Deploying the contracts with the account:",
    await deployer.getAddress()
  );

  const TransactionV3 = await ethers.getContractFactory("TransactionV3");
  const transaction = await upgrades.upgradeProxy(BOX_ADDRESS, TransactionV3);
  await transaction.deployed();

  console.log("TransactionV3 address:", transaction.address);
  const _balanceEther = ethers.utils
    .formatEther(await deployer.getBalance())
    .toString();
  console.log("Deployer balance:", _balanceEther);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
