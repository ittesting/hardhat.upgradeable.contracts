const { ethers, upgrades } = require("hardhat");

async function main() {
  const BOX_ADDRESS = "0x5FC8d32690cc91D4c39d9d3abcBD16989F875707";
  const TransactionV3 = await ethers.getContractFactory("TransactionV3");
  const transaction = await TransactionV3.attach(BOX_ADDRESS);

  await transaction.initialize();
  const data = await transaction.getStr();
  console.log('Str: ', data.toString());
}
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
