const { ethers, upgrades } = require("hardhat");

async function main() {
    // Retrieve accounts from the local node
    const accounts = await ethers.provider.listAccounts();
    console.log(accounts);

    const address = '0x5FC8d32690cc91D4c39d9d3abcBD16989F875707'; // BOX_ADDRESS
    const TransactionV2 = await ethers.getContractFactory('TransactionV2');
    const transactionV2 = await TransactionV2.attach(address);
    // initialize a value
    await transactionV2.store(500);

    // initialize a value
    // await transactionV2.initialize(50);
    
    const retrieve = await transactionV2.retrieve();
    // Test if the returned value is the same one
    // Note that we need to use strings to compare the 256 bit integers
    console.log(retrieve.toString());
}
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });


