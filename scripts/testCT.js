const { ethers, upgrades } = require("hardhat");
require('dotenv').config();
const { CT_ADDRESS } = process.env;

async function main() {
  // console.log("Deploying...");
  const FreeTextContract = await ethers.getContractFactory("FreeTextContract");
  const transactionTransaction1 = await FreeTextContract.attach(CT_ADDRESS);
  const dataChecking1 = await transactionTransaction1.getFreeTextContract();
  console.log("FreeText: ", dataChecking1.toString());
  // const freeTextContract = await upgrades.deployProxy(FreeTextContract, [], {
  //   initializer: "initializeFreeTextContract",
  // });
  // await freeTextContract.deployed();

  // CT_ADDRESS = freeTextContract.address;

  // const transaction = await FreeTextContract.attach(CT_ADDRESS);
    
  // const data = await transaction.getFreeTextContract();
  // console.log("FreeText: ", data.toString());

  console.log('Upgrading...');
  const FreeTextContractV1 = await ethers.getContractFactory(
    "FreeTextContractV1"
  );
  const freeTextContractV1 = await upgrades.upgradeProxy(
    CT_ADDRESS,
    FreeTextContractV1
  );
  await freeTextContractV1.deployed();
  await freeTextContractV1.initializeFreeTextContractV2();

  const transaction1 = await FreeTextContractV1.attach(CT_ADDRESS);
  const data1 = await transaction1.getFreeTextContract();
  console.log("FreeText: ", data1.toString());
  const FreeTextV1 = await transaction1.getFreeTextContractV1();
  console.log("FreeTextV1: ", FreeTextV1.toString());

  console.log("Checking changing...");
  const transactionTransaction = await FreeTextContract.attach(CT_ADDRESS);
  const dataChecking = await transactionTransaction.getFreeTextContract();
  console.log("FreeText: ", dataChecking.toString());

  console.log("Done...");
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
