const { ethers, upgrades } = require("hardhat");
require('dotenv').config();
const { CT_ADDRESS } = process.env;
async function main() {
  const TransactionV3 = await ethers.getContractFactory("FreeTextContract");
  const transaction = await TransactionV3.attach(CT_ADDRESS);

  const data = await transaction.getFreeTextContract();
  console.log('FreeText: ', data.toString());
}
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
