const { ethers, upgrades } = require("hardhat");
require('dotenv').config();
const { CT_ADDRESS } = process.env;
async function main() {
  const TransactionV3 = await ethers.getContractFactory("FreeTextContractV1");
  const transaction = await TransactionV3.attach(CT_ADDRESS);

  // await transaction.initializeFreeTextContractV2();
  const data = await transaction.getFreeTextContract();
  console.log('FreeText: ', data.toString());
  const FreeTextV1 = await transaction.getFreeTextContractV1();
  console.log('FreeTextV1: ', FreeTextV1.toString());
}
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
