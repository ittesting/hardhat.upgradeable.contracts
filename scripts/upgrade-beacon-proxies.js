const { ethers, upgrades } = require("hardhat");
require("dotenv").config();
const { CT_ADDRESS, BEACON_ADDRESS, BOX_ADDRESS } = process.env;

async function main() {
  const BeaconProxyPatternV2 = await ethers.getContractFactory(
    "BeaconProxyPatternV2"
  );

  await upgrades.upgradeBeacon(BEACON_ADDRESS, BeaconProxyPatternV2,{ initializer: "initialize" });
  console.log("Beacon upgraded");

  const box = BeaconProxyPatternV2.attach(BOX_ADDRESS);
  console.log(await box.getContractNameWithVersion());
}

main();
