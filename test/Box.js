// test/Box.js
// Load dependencies
const { expect } = require('chai');
const { hre } = require("hardhat");
 
// let Box;
// let box;
 
// // Start test block
// describe('Box', function () {
//   beforeEach(async function () {
//     Box = await ethers.getContractFactory("TransactionV2");
//     box = await Box.deploy();
//     await box.deployed();
//   });
 
//   // Test case
//   it('retrieve returns a value previously initialize', async function () {
//     // initialize a value
//     await box.initialize(42);
    
//     const retrieve = await box.retrieve();
//     // Test if the returned value is the same one
//     // Note that we need to use strings to compare the 256 bit integers
//     console.log(retrieve);
//     expect((await box.retrieve()).toString()).to.equal('42');
//   });
// });

describe("suite", function () {
  beforeEach(async function () {
    await hre.network.provider.send("hardhat_reset")
  })
});